package com.vuitv.myapplication.Object;

import android.os.Parcel;
import android.os.Parcelable;

public class XeBus implements Parcelable {
    public static final Creator<XeBus> CREATOR = new Creator<XeBus>() {
        @Override
        public XeBus createFromParcel(Parcel in) {
            return new XeBus(in);
        }

        @Override
        public XeBus[] newArray(int size) {
            return new XeBus[size];
        }
    };
    private String ma;
    private String tenxe;
    private String xinghiep;
    private String tanxuat;
    private String giave;
    private String soxe;
    private String thoigian;
    private String chieudi;
    private String chieuve;
    private String polyline;

    public XeBus() {
    }

    public XeBus(String tenxe) {
        this.tenxe = tenxe;
    }

    public XeBus(String ma, String tenxe) {
        this.ma = ma;
        this.tenxe = tenxe;
    }

    public XeBus(String ma, String tenxe, String xinghiep, String tanxuat, String giave, String soxe, String thoigian, String chieudi, String chieuve, String polyline) {
        this.ma = ma;
        this.tenxe = tenxe;
        this.xinghiep = xinghiep;
        this.tanxuat = tanxuat;
        this.giave = giave;
        this.soxe = soxe;
        this.thoigian = thoigian;
        this.chieudi = chieudi;
        this.chieuve = chieuve;
        this.polyline = polyline;
    }

    protected XeBus(Parcel in) {
        ma = in.readString();
        tenxe = in.readString();
        xinghiep = in.readString();
        tanxuat = in.readString();
        giave = in.readString();
        soxe = in.readString();
        thoigian = in.readString();
        chieudi = in.readString();
        chieuve = in.readString();
        polyline = in.readString();
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTenxe() {
        return tenxe;
    }

    public void setTenxe(String tenxe) {
        this.tenxe = tenxe;
    }

    public String getXinghiep() {
        return xinghiep;
    }

    public void setXinghiep(String xinghiep) {
        this.xinghiep = xinghiep;
    }

    public String getTanxuat() {
        return tanxuat;
    }

    public void setTanxuat(String tanxuat) {
        this.tanxuat = tanxuat;
    }

    public String getGiave() {
        return giave;
    }

    public void setGiave(String giave) {
        this.giave = giave;
    }

    public String getSoxe() {
        return soxe;
    }

    public void setSoxe(String soxe) {
        this.soxe = soxe;
    }

    public String getThoigian() {
        return thoigian;
    }

    public void setThoigian(String thoigian) {
        this.thoigian = thoigian;
    }

    public String getChieudi() {
        return chieudi;
    }

    public void setChieudi(String chieudi) {
        this.chieudi = chieudi;
    }

    public String getChieuve() {
        return chieuve;
    }

    public void setChieuve(String chieuve) {
        this.chieuve = chieuve;
    }

    public String getPolyline() {
        return polyline;
    }

    public void setPolyline(String polyline) {
        this.polyline = polyline;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(ma);
        parcel.writeString(tenxe);
        parcel.writeString(xinghiep);
        parcel.writeString(tanxuat);
        parcel.writeString(giave);
        parcel.writeString(soxe);
        parcel.writeString(thoigian);
        parcel.writeString(chieudi);
        parcel.writeString(chieuve);
        parcel.writeString(polyline);
    }
}
