package com.vuitv.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vuitv.myapplication.Adapter.FavoritePlaceAdapter;
import com.vuitv.myapplication.Object.DiemDon;
import com.vuitv.myapplication.Object.Dijkstra;
import com.vuitv.myapplication.Object.FavoritePlace;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

public class HomeFragment extends Fragment {

    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_EDIT = 2;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_ADD = 3;

    Spinner spDiemDi, spDiemDen, spTuyChon;
    EditText etMin, etMax;
    TextView tvResult;
    ImageView btAddPlace;
    String stData;
    ArrayList<FavoritePlace> lsPlace;
    FavoritePlaceAdapter adapter;
    int position;
    Dijkstra dj;

    static String diemdi = null;
    static String diemden = null;
    static int tuychon = -1;
    static int min = -1;
    static int max = -1;

    public HomeFragment() {
        // Required empty public constructor
    }

    private void processSelection(String diemdi, String diemden, int tuychon, int min, int max) {
        if (diemdi == null || diemden == null || tuychon == -1) {
            return;
        }
        DiemDon di = MainActivity.mpDiemDon.getOrDefault(diemdi.split("\\[")[0].trim(), null);
        DiemDon den = MainActivity.mpDiemDon.getOrDefault(diemden.split("\\[")[0].trim(), null);
        if (di == null || den == null) {
            return;
        }
        String duong = dj.processSelection(getActivity().getApplicationContext(), di, den, tuychon, min, max);
        tvResult.setText(duong);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        dj = new Dijkstra();

        tvResult = view.findViewById(R.id.tvDataResult);

        spDiemDi = view.findViewById(R.id.spLabelFrom);
        spDiemDi.setAdapter(new ArrayAdapter<>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, MainActivity.lsPoint.toArray()));
        spDiemDi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                diemdi = adapterView.getItemAtPosition(i).toString();
                processSelection(diemdi, diemden, tuychon, min, max);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spDiemDen = view.findViewById(R.id.spLabelTo);
        spDiemDen.setAdapter(new ArrayAdapter<>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, MainActivity.lsPoint.toArray()));
        spDiemDen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                diemden = adapterView.getItemAtPosition(i).toString();
                processSelection(diemdi, diemden, tuychon, min, max);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spTuyChon = view.findViewById(R.id.spLabelPreference);
        spTuyChon.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                tuychon = i;
                processSelection(diemdi, diemden, tuychon, min, max);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        etMin = view.findViewById(R.id.etLabelMin);
        etMin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                min = Integer.parseInt(charSequence.toString());
                ((MainActivity)getActivity()).reloadData(min, max);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etMin.setText(Integer.toString(min));

        etMax = view.findViewById(R.id.etLabelMax);
        etMax.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                max = Integer.parseInt(charSequence.toString());
                ((MainActivity)getActivity()).reloadData(min, max);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etMax.setText(Integer.toString(max));

//        tvLable.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                btnTimkiem.startAnimation(animZoomOut);
//                try {
//                    Intent intent = new PlaceAutocomplete.IntentBuilder(
//                            PlaceAutocomplete.MODE_FULLSCREEN).build(getActivity());
//                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
//                } catch (GooglePlayServicesRepairableException e) {
//                    // TODO: Handle the error.
//                } catch (GooglePlayServicesNotAvailableException e) {
//                    // TODO: Handle the error.
//                }
//            }
//        });

//        btnTimkiem.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                btnTimkiem.startAnimation(animZoomOut);
//                try {
//                    Intent intent = new PlaceAutocomplete.IntentBuilder(
//                            PlaceAutocomplete.MODE_FULLSCREEN).build(getActivity());
//                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
//                } catch (GooglePlayServicesRepairableException e) {
//                } catch (GooglePlayServicesNotAvailableException e) {
//                }
//            }
//        });
        creatData();
        adapter = new FavoritePlaceAdapter(lsPlace, getActivity());
//        lsView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
//        lsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                FavoritePlace favoritePlace = lsPlace.get(i);
//                if (favoritePlace.getLatLng() == null) {
//                    try {
//                        Intent intent = new PlaceAutocomplete.IntentBuilder(
//                                PlaceAutocomplete.MODE_FULLSCREEN).build(getActivity());
//                        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_EDIT);
//                    } catch (GooglePlayServicesRepairableException e) {
//                        e.printStackTrace();
//                    } catch (GooglePlayServicesNotAvailableException e) {
//                        e.printStackTrace();
//                    }
//                    position = i;
//                } else {
//                    Intent myIntent = new Intent(getActivity(), FindPathActivity.class);
//                    myIntent.putExtra("id", favoritePlace.getId());
//                    myIntent.putExtra("name", favoritePlace.getAddress());
//                    myIntent.putExtra("lat", favoritePlace.getLatLng().latitude);
//                    myIntent.putExtra("lng", favoritePlace.getLatLng().longitude);
//                    getActivity().startActivity(myIntent);
//                }
//            }
//        });

//        btAddPlace.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                try {
//                    Intent intent = new PlaceAutocomplete.IntentBuilder(
//                            PlaceAutocomplete.MODE_FULLSCREEN).build(getActivity());
//                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_ADD);
//                } catch (GooglePlayServicesRepairableException e) {
//                } catch (GooglePlayServicesNotAvailableException e) {
//                }
//            }
//        });

        return view;
    }

    public void creatData() {
        final SharedPreferences prefs = getActivity().getSharedPreferences("MyShare", Context.MODE_PRIVATE);
        stData = prefs.getString("stData", "");
        Type listType = new TypeToken<ArrayList<FavoritePlace>>() {
        }.getType();
        if (stData.equalsIgnoreCase("")) {
            lsPlace = new ArrayList<>();
            lsPlace.add(new FavoritePlace("Nhà", FavoritePlace.TYPE_HOME));
            lsPlace.add(new FavoritePlace("Công ty", FavoritePlace.TYPE_COMPANY));
            stData = new Gson().toJson(lsPlace, listType);
            prefs.edit().putString("stData", stData).commit();
        } else {
            lsPlace = new Gson().fromJson(stData, listType);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        final SharedPreferences prefs = getActivity().getSharedPreferences("MyShare", Context.MODE_PRIVATE);
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                Intent myIntent = new Intent(getActivity(), FindPathActivity.class);
                myIntent.putExtra("id", place.getId().toString());
                myIntent.putExtra("name", place.getName().toString());
                myIntent.putExtra("lat", place.getLatLng().latitude);
                myIntent.putExtra("lng", place.getLatLng().longitude);
                getActivity().startActivity(myIntent);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
            }
        }
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_EDIT) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                if (position == 1) {
                    lsPlace.set(position, new FavoritePlace("Công ty",
                            place.getAddress().toString().substring(0, place.getAddress().toString().indexOf(", ")),
                            place.getId(), place.getLatLng(), FavoritePlace.TYPE_COMPANY));
                } else
                    lsPlace.set(position, new FavoritePlace("Nhà",
                            place.getAddress().toString().substring(0, place.getAddress().toString().indexOf(", ")),
                            place.getId(), place.getLatLng(), FavoritePlace.TYPE_HOME));
                adapter.notifyDataSetChanged();
                Type type = new TypeToken<ArrayList<FavoritePlace>>() {
                }.getType();
                stData = new Gson().toJson(lsPlace, type);
                prefs.edit().putString("stData", stData).commit();
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
            }

        }
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_ADD) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                FavoritePlace favoritePlace = new FavoritePlace(place.getName().toString(),
                        place.getAddress().toString().substring(0, place.getAddress().toString().indexOf(", ")),
                        place.getId(), place.getLatLng(), FavoritePlace.TYPE_OTHER);
                lsPlace.add(favoritePlace);
                adapter.notifyDataSetChanged();
                Type type = new TypeToken<ArrayList<FavoritePlace>>() {
                }.getType();
                stData = new Gson().toJson(lsPlace, type);
                prefs.edit().putString("stData", stData).commit();
            }
        }
    }

    public String getData(String strUrl) {
        String strResult = "";
        URL url;
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        try {
            url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setConnectTimeout(60000);
            InputStream in = urlConnection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                strResult += line;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return strResult;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public void hideSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        hideSoftKeyboard();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    public class LoadData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String strResult = "";
            String strUrl = params[0];
            strResult = getData(strUrl);
            return strResult;
        }

        @Override
        protected void onPostExecute(String s) {
            final String data = s;
            try {
                JSONObject jsData = new JSONObject(data);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            super.onPostExecute(s);
        }
    }
}
