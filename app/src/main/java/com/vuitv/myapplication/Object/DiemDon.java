package com.vuitv.myapplication.Object;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class DiemDon implements Parcelable {
    public static final Creator<DiemDon> CREATOR = new Creator<DiemDon>() {
        @Override
        public DiemDon createFromParcel(Parcel in) {
            return new DiemDon(in);
        }

        @Override
        public DiemDon[] newArray(int size) {
            return new DiemDon[size];
        }
    };
    private String tendiem;
    private Set<String> tuyenden;
    private Set<String> tuyendi;
    private Set<DiemDon> diden;
    private Set<DiemDon> ditu;

    public DiemDon() {
    }

    public DiemDon(String tendiem) {
        this.tendiem = tendiem;
        this.tuyenden = new HashSet<>();
        this.tuyendi = new HashSet<>();
        this.diden = new HashSet<>();
        this.ditu = new HashSet<>();
    }

    protected DiemDon(Parcel in) {

    }

    public String getTendiem() {
        return tendiem;
    }

    public void setTendiem(String tendiem) {
        this.tendiem = tendiem;
    }

    public Set<String> getTuyenden() {
        return tuyenden;
    }

    public void addTuyenden(String tuyenxe) {
        this.tuyenden.add(tuyenxe);
    }

    public  Set<String> getTuyendi() {
        return tuyendi;
    }

    public void addTuyendi(String tuyenxe) {
        this.tuyendi.add(tuyenxe);
    }

    public Set<DiemDon> getDiden() {
        return diden;
    }

    public void addDiden(DiemDon diden) {
        this.diden.add(diden);
    }

    public Set<DiemDon> getDitu() {
        return ditu;
    }

    public void addDitu(DiemDon ditu) {
        this.ditu.add(ditu);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(tendiem);
    }
}
