package com.vuitv.myapplication.Object.directions;

import android.os.Parcel;
import android.os.Parcelable;

public class ArrivalTime implements Parcelable {
    public static final Creator<ArrivalTime> CREATOR = new Creator<ArrivalTime>() {
        @Override
        public ArrivalTime createFromParcel(Parcel in) {
            return new ArrivalTime(in);
        }

        @Override
        public ArrivalTime[] newArray(int size) {
            return new ArrivalTime[size];
        }
    };
    private String text;
    private int value;

    public ArrivalTime(String text, int value) {
        this.text = text;
        this.value = value;
    }

    protected ArrivalTime(Parcel in) {
        text = in.readString();
        value = in.readInt();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(text);
        parcel.writeInt(value);
    }
}
