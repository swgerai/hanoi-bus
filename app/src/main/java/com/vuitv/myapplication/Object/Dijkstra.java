package com.vuitv.myapplication.Object;

import android.content.Context;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Dijkstra {
    private class Dot {
        public DiemDon diem;
        public Dot qua;
        public Set<Dot> toi;
        public int xa;

        public Dot(DiemDon diem) {
            this.diem = diem;
            toi = new HashSet<>();
        }

        public void updateDistant(int xa) {
            this.xa = xa;
            if (diem == dich) {
                return;
            }

            for (Dot t : toi) {
                if (xa + 1 < t.xa) {
                    t.qua = this;
                    t.updateDistant(xa + 1);
                }
            }
        }
    }
    private Map<String, Dot> mpDanhSach;
    private Dot dotDich;
    private DiemDon dich;

    public Dijkstra() {
        mpDanhSach = new HashMap<>();
        dich = null;
    }

    private void processDot(Dot di) {
        for (DiemDon den: di.diem.getDiden()) {
            Dot d = mpDanhSach.getOrDefault(den.getTendiem(), null);

            if (d == null) {
                // Diem moi
                d = new Dot(den);
                d.qua = di;
                di.toi.add(d);
                d.xa = di.xa + 1;
                mpDanhSach.put(d.diem.getTendiem(), d);

                if (den != dich) {
                    // Di tiep
                    processDot(d);
                } else {
                    dotDich = d;
                }
            } else if (di.xa + 1 < d.xa) {
                //d.qua.toi.remove(d);
                d.qua = di;
                di.toi.add(d);
                d.updateDistant(di.xa + 1);
            }
        }
    }

    public String processSelection(Context ctx, DiemDon di, DiemDon den, int tuychon, int min, int max) {
        //Toast.makeText(ctx, di.getTendiem() + " => " + den.getTendiem() + " [" + sapxep + " - " + tuychon + "]", Toast.LENGTH_LONG).show();

        if (di == den) {
            return "KHOI DI";
        }

        mpDanhSach = new HashMap<>();
        dotDich = null;
        dich = den;
        Dot dotDi = new Dot(di);
        dotDi.qua = null;
        dotDi.xa = 0;

        mpDanhSach.put(di.getTendiem(), dotDi);
        processDot(dotDi);

        StringBuilder sb = new StringBuilder();
        Set<String> cacTuyen = new HashSet<>();
        String tuyenChinh = null;


        if (dotDich != null) {
            Dot dotTemp = dotDich;
            while (dotTemp.qua != null) {
                boolean doiTuyen = false;
                cacTuyen.clear();
                for ( String tuyenDen : dotTemp.diem.getTuyenden()) {
                    for (String tuyenDi : dotTemp.qua.diem.getTuyendi()) {
                        if (tuyenDen.equals(tuyenDi)) {
                            cacTuyen.add(tuyenDen);
                        }
                    }
                }
                if (tuyenChinh == null || !cacTuyen.contains(tuyenChinh)) {
                    tuyenChinh = cacTuyen.iterator().next();
                    doiTuyen = true;
                }

                if (tuychon != 1 || doiTuyen) {
                    sb.insert(0, ")");
                    sb.insert(0, dotTemp.xa);
                    sb.insert(0, "(");
                    sb.insert(0, dotTemp.diem.getTendiem());
                    sb.insert(0, "]=> ");
                    sb.insert(0, tuyenChinh);
                    sb.insert(0, " =[");
                }
                dotTemp = dotTemp.qua;
            }
        }
        //Toast.makeText(ctx, sb.toString(), Toast.LENGTH_LONG).show();
        return sb.toString().trim();
    }
}
