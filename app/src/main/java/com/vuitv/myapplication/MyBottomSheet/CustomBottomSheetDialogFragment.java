package com.vuitv.myapplication.MyBottomSheet;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.vuitv.myapplication.FindPathActivity;
import com.vuitv.myapplication.R;

import java.util.ArrayList;

public class CustomBottomSheetDialogFragment extends BottomSheetDialogFragment {

    public static final String FRAGMENT_KEY = "com.vuitv.myapplication.MyBottomSheet.CustomBottomSheetDialogFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.bottom_sheet_option, container);

        ArrayList<ItemBottomSheet> items = getArguments().getParcelableArrayList(ItemBottomSheet.ITEMS_KEY);

        ActionAdapter adapter = new ActionAdapter(getActivity(), items);

        ListView lv = (ListView) view.findViewById(R.id.lv_actions);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FindPathActivity callingActivity = (FindPathActivity) getActivity();
                callingActivity.onResultDialogFragment(position);
                dismiss();
            }
        });

        return (view);
    }
}
