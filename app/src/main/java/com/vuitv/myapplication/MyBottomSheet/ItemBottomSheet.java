package com.vuitv.myapplication.MyBottomSheet;

import android.os.Parcel;
import android.os.Parcelable;

public class ItemBottomSheet implements Parcelable {

    public static final String ITEMS_KEY = "com.vuitv.myapplication.MyBottomSheet.ItemBottomSheet";
    public static final Creator<ItemBottomSheet> CREATOR = new Creator<ItemBottomSheet>() {
        @Override
        public ItemBottomSheet createFromParcel(Parcel in) {
            return new ItemBottomSheet(in);
        }

        @Override
        public ItemBottomSheet[] newArray(int size) {
            return new ItemBottomSheet[size];
        }
    };
    private int iconId;
    private String label;

    public ItemBottomSheet(int iconId, String label) {
        this.iconId = iconId;
        this.label = label;
    }

    protected ItemBottomSheet(Parcel in) {
        this.iconId = in.readInt();
        this.label = in.readString();
    }

    public int getIconId() {
        return (this.iconId);
    }

    public String getLabel() {
        return label;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.iconId);
        parcel.writeString(this.label);
    }
}
