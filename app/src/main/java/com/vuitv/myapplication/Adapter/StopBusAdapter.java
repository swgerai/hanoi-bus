package com.vuitv.myapplication.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.vuitv.myapplication.R;

import java.util.List;

public class StopBusAdapter extends BaseAdapter {

    List<String> lsData;
    LayoutInflater inflater;

    public StopBusAdapter() {
    }

    public StopBusAdapter(List<String> lsData, Context context) {
        this.lsData = lsData;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return lsData.size();
    }

    @Override
    public Object getItem(int i) {
        return lsData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null)
            view = inflater.inflate(R.layout.item_maps_bus, viewGroup, false);

        TextView tvName = (TextView) view.findViewById(R.id.tvName);
        String s = lsData.get(i);
        tvName.setText(s);
        return view;
    }
}
