package com.vuitv.myapplication.MyBottomSheet;

import android.os.Parcel;
import android.os.Parcelable;

public class Action extends ItemBottomSheet {

    public static final Parcelable.Creator<Action> CREATOR = new Parcelable.Creator<Action>() {
        public Action createFromParcel(Parcel source) {
            return new Action(source);
        }

        public Action[] newArray(int size) {
            return new Action[size];
        }
    };

    public Action(int iconId, String label) {
        super(iconId, label);
    }

    protected Action(Parcel in) {
        super(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }
}
