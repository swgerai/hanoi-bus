package com.vuitv.myapplication.MyBottomSheet;

import com.vuitv.myapplication.R;

import java.util.ArrayList;


public final class ItemBottomSheetCollection {
    static private ArrayList<Action> actions;
    static private ArrayList<Action> actionsWhite;

    private ItemBottomSheetCollection() {
    }

    static public ArrayList<Action> getActions() {
        if (actions == null) {
            actions = new ArrayList<>();
            actions.add(new Action(R.drawable.ic_linear_scale_blue_24px, "Tuyến tốt nhất"));
            actions.add(new Action(R.drawable.ic_walk_blue_24dp, "Đi bộ ít nhất"));
            actions.add(new Action(R.drawable.ic_directions_bus_blue_24px, "Chuyển tuyến ít"));
        }
        return (actions);
    }

    static public ArrayList<Action> getActionsWhite() {
        if (actionsWhite == null) {
            actionsWhite = new ArrayList<>();
            actionsWhite.add(new Action(R.drawable.ic_linear_scale_white_24px, "Tuyến tốt nhất"));
            actionsWhite.add(new Action(R.drawable.ic_walk_white_24dp, "Đi bộ ít nhất"));
            actionsWhite.add(new Action(R.drawable.ic_directions_bus_white_24px, "Chuyển tuyến ít"));


        }
        return (actionsWhite);
    }
}
