package com.vuitv.myapplication.Object;

import com.google.android.gms.maps.model.LatLng;

public class FavoritePlace {
    public static final int TYPE_OTHER = 0;
    public static final int TYPE_HOME = 1;
    public static final int TYPE_COMPANY = 2;

    private String name;
    private String address;
    private String id;
    private LatLng latLng;
    private int type;

    public FavoritePlace() {
    }

    public FavoritePlace(String name, int type) {
        this.name = name;
        this.type = type;
    }

    public FavoritePlace(String name, String address, String id, LatLng latLng, int type) {
        this.name = name;
        this.address = address;
        this.id = id;
        this.latLng = latLng;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
