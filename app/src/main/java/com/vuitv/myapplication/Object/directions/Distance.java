package com.vuitv.myapplication.Object.directions;

import android.os.Parcel;
import android.os.Parcelable;

public class Distance implements Parcelable {
    public static final Creator<Distance> CREATOR = new Creator<Distance>() {
        @Override
        public Distance createFromParcel(Parcel in) {
            return new Distance(in);
        }

        @Override
        public Distance[] newArray(int size) {
            return new Distance[size];
        }
    };
    private String text;
    private int value;

    public Distance() {
    }

    public Distance(String text, int value) {
        this.text = text;
        this.value = value;
    }

    protected Distance(Parcel in) {
        text = in.readString();
        value = in.readInt();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(text);
        parcel.writeInt(value);
    }

}
