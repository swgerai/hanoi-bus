package com.vuitv.myapplication.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.vuitv.myapplication.Object.FavoritePlace;
import com.vuitv.myapplication.R;

import java.util.ArrayList;

public class FavoritePlaceAdapter extends BaseAdapter {
    ArrayList<FavoritePlace> listData;
    LayoutInflater inflater;

    public FavoritePlaceAdapter() {
    }

    public FavoritePlaceAdapter(ArrayList<FavoritePlace> listData, Context context) {
        this.listData = listData;
        this.inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int i) {
        return listData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null)
            view = inflater.inflate(R.layout.item_home, viewGroup, false);

        FavoritePlace favoritePlace = listData.get(i);
        TextView textView = (TextView) view.findViewById(R.id.textView);
        textView.setText(favoritePlace.getName());
        TextView textView2 = (TextView) view.findViewById(R.id.textView2);
        ImageView imageView = (ImageView) view.findViewById(R.id.image);

        if (favoritePlace.getType() == FavoritePlace.TYPE_HOME) {
            imageView.setImageResource(R.drawable.ic_home_black_36px);
        } else if (favoritePlace.getType() == FavoritePlace.TYPE_COMPANY) {
            imageView.setImageResource(R.drawable.ic_business_center_black_36px);
        } else if (favoritePlace.getType() == FavoritePlace.TYPE_OTHER) {
            imageView.setImageResource(R.drawable.ic_star_black_36px);
            textView.setText(favoritePlace.getName());
        }
        if (!(favoritePlace.getAddress() == null))
            textView2.setText(favoritePlace.getAddress());
        else
            textView2.setText("Chạm để sửa");
        return view;
    }
}
