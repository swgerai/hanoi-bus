package com.vuitv.myapplication.Object.directions;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public class Routes implements Parcelable {
    public static final Creator<Routes> CREATOR = new Creator<Routes>() {
        @Override
        public Routes createFromParcel(Parcel in) {
            return new Routes(in);
        }

        @Override
        public Routes[] newArray(int size) {
            return new Routes[size];
        }
    };
    private Fare fare;
    private ArrivalTime arrivalTime;
    private ArrivalTime departureTime;
    private Distance distance;
    private Duration duration;
    private String endAddress;
    private LatLng endLocation;
    private String startAddress;
    private LatLng startLocation;
    private List<Steps> steps;
    private List<LatLng> points;

    public Routes() {
    }

    protected Routes(Parcel in) {
        arrivalTime = in.readParcelable(ArrivalTime.class.getClassLoader());
        departureTime = in.readParcelable(ArrivalTime.class.getClassLoader());
        distance = in.readParcelable(Distance.class.getClassLoader());
        duration = in.readParcelable(Duration.class.getClassLoader());
        endAddress = in.readString();
        endLocation = in.readParcelable(LatLng.class.getClassLoader());
        startAddress = in.readString();
        startLocation = in.readParcelable(LatLng.class.getClassLoader());
        steps = in.createTypedArrayList(Steps.CREATOR);
        points = in.createTypedArrayList(LatLng.CREATOR);
    }

    public static Creator<Routes> getCREATOR() {
        return CREATOR;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(arrivalTime, i);
        parcel.writeParcelable(departureTime, i);
        parcel.writeParcelable(distance, i);
        parcel.writeParcelable(duration, i);
        parcel.writeString(endAddress);
        parcel.writeParcelable(endLocation, i);
        parcel.writeString(startAddress);
        parcel.writeParcelable(startLocation, i);
        parcel.writeTypedList(steps);
        parcel.writeTypedList(points);
    }

    public Fare getFare() {
        return fare;
    }

    public void setFare(Fare fare) {
        this.fare = fare;
    }

    public ArrivalTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(ArrivalTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public ArrivalTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(ArrivalTime departureTime) {
        this.departureTime = departureTime;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public String getEndAddress() {
        return endAddress;
    }

    public void setEndAddress(String endAddress) {
        this.endAddress = endAddress;
    }

    public LatLng getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(LatLng endLocation) {
        this.endLocation = endLocation;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public LatLng getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(LatLng startLocation) {
        this.startLocation = startLocation;
    }

    public List<Steps> getSteps() {
        return steps;
    }

    public void setSteps(List<Steps> steps) {
        this.steps = steps;
    }

    public List<LatLng> getPoints() {
        return points;
    }

    public void setPoints(List<LatLng> points) {
        this.points = points;
    }
}
