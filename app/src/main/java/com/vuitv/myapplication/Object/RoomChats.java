package com.vuitv.myapplication.Object;

import android.os.Parcel;
import android.os.Parcelable;

public class RoomChats implements Parcelable {
    public static final Creator<RoomChats> CREATOR = new Creator<RoomChats>() {
        @Override
        public RoomChats createFromParcel(Parcel in) {
            return new RoomChats(in);
        }

        @Override
        public RoomChats[] newArray(int size) {
            return new RoomChats[size];
        }
    };
    private String storage;
    private String subtitle;
    private String title;
    private String type;

    public RoomChats() {
    }

    public RoomChats(String storage, String subtitle, String title, String type) {
        this.storage = storage;
        this.subtitle = subtitle;
        this.title = title;
        this.type = type;
    }

    protected RoomChats(Parcel in) {
        storage = in.readString();
        subtitle = in.readString();
        title = in.readString();
        type = in.readString();
    }

    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(storage);
        parcel.writeString(subtitle);
        parcel.writeString(title);
        parcel.writeString(type);
    }
}
