package com.vuitv.myapplication.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vuitv.myapplication.Object.directions.Routes;
import com.vuitv.myapplication.Object.directions.Steps;
import com.vuitv.myapplication.R;

import java.util.ArrayList;
import java.util.List;

public class ResultRoutesAdapter extends BaseAdapter {

    ArrayList<Routes> listData;
    LayoutInflater inflater;

    public ResultRoutesAdapter(ArrayList<Routes> listData, Context context) {
        this.listData = listData;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_direction, parent, false);
        }
        LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.llRoutes);
        Routes routes = listData.get(position);

        List<Steps> lsSteps = routes.getSteps();
        for (int j = 0; j < lsSteps.size(); j++) {
            Steps steps = lsSteps.get(j);

            if (steps.getTravelMode().equals("WALKING")) {
                TextView txtWalking = new TextView(convertView.getContext());
                txtWalking.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_walk_blue_24dp, 0, 0, 0);
                txtWalking.setText(steps.getDuration().getText().substring(0, 1));
                txtWalking.setGravity(Gravity.CENTER);
                txtWalking.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
                linearLayout.addView(txtWalking);
            }
            if (steps.getTravelMode().equals("TRANSIT")) {
                TextView txtTransit = new TextView(convertView.getContext());
                txtTransit.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_directions_bus_grey_24px, 0, 0, 0);
                txtTransit.setBackgroundResource(R.drawable.shape_bus);
                txtTransit.setCompoundDrawablePadding(3);
                txtTransit.setText(steps.getTransitDetails().getLineName().substring(0, 3));
                txtTransit.setGravity(Gravity.CENTER);
                txtTransit.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
                txtTransit.setTextSize(18);
                txtTransit.setTypeface(Typeface.DEFAULT_BOLD);
                linearLayout.addView(txtTransit);
            }

            if (j < lsSteps.size() - 1) {
                ImageView img = new ImageView(convertView.getContext());
                img.setImageResource(R.drawable.ic_chevron_right_gray_24dp);
                img.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
                linearLayout.addView(img);
            }

        }

        TextView tvThoiGian, tvHTML, tvTime;
        tvThoiGian = (TextView) convertView.findViewById(R.id.tvThoigian);
        tvHTML = (TextView) convertView.findViewById(R.id.tvHTML);
        tvTime = (TextView) convertView.findViewById(R.id.tvTime);

        for (int j = 0; j < lsSteps.size(); j++) {
            Steps steps = lsSteps.get(j);
            if (steps.getTravelMode().equals("TRANSIT")) {
                tvHTML.setText("Rời bến từ " + routes.getSteps().get(j).getTransitDetails().getDepartureStop().getName());
                break;
            }
        }
        tvThoiGian.setText("" + (routes.getDuration().getValue() / 60));
        tvTime.setText(routes.getDepartureTime().getText() + " - " + routes.getArrivalTime().getText());
        return convertView;
    }
}

