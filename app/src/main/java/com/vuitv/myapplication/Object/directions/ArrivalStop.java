package com.vuitv.myapplication.Object.directions;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

public class ArrivalStop implements Parcelable {
    public static final Creator<ArrivalStop> CREATOR = new Creator<ArrivalStop>() {
        @Override
        public ArrivalStop createFromParcel(Parcel in) {
            return new ArrivalStop(in);
        }

        @Override
        public ArrivalStop[] newArray(int size) {
            return new ArrivalStop[size];
        }
    };
    private LatLng location;
    private String name;

    public ArrivalStop(LatLng location, String name) {
        this.location = location;
        this.name = name;
    }

    protected ArrivalStop(Parcel in) {
        location = in.readParcelable(LatLng.class.getClassLoader());
        name = in.readString();
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(location, i);
        parcel.writeString(name);
    }
}
