package com.vuitv.myapplication.Object.directions;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public class Steps implements Parcelable {
    public static final Creator<Steps> CREATOR = new Creator<Steps>() {
        @Override
        public Steps createFromParcel(Parcel in) {
            return new Steps(in);
        }

        @Override
        public Steps[] newArray(int size) {
            return new Steps[size];
        }
    };
    private Distance distance;
    private Duration duration;
    private LatLng endLocation;
    private String htmlInstructions;
    private LatLng startLocation;
    private TransitDetails transitDetails;
    private String travelMode;
    private List<LatLng> points;
    private List<Steps> step;


    public Steps() {
    }

    protected Steps(Parcel in) {
        distance = in.readParcelable(Distance.class.getClassLoader());
        duration = in.readParcelable(Duration.class.getClassLoader());
        endLocation = in.readParcelable(LatLng.class.getClassLoader());
        htmlInstructions = in.readString();
        startLocation = in.readParcelable(LatLng.class.getClassLoader());
        transitDetails = in.readParcelable(TransitDetails.class.getClassLoader());
        travelMode = in.readString();
        points = in.createTypedArrayList(LatLng.CREATOR);
        step = in.createTypedArrayList(Steps.CREATOR);
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public LatLng getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(LatLng endLocation) {
        this.endLocation = endLocation;
    }

    public String getHtmlInstructions() {
        return htmlInstructions;
    }

    public void setHtmlInstructions(String htmlInstructions) {
        this.htmlInstructions = htmlInstructions;
    }

    public LatLng getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(LatLng startLocation) {
        this.startLocation = startLocation;
    }

    public List<LatLng> getPoints() {
        return points;
    }

    public void setPoints(List<LatLng> points) {
        this.points = points;
    }

    public List<Steps> getStep() {
        return step;
    }

    public void setStep(List<Steps> step) {
        this.step = step;
    }

    public TransitDetails getTransitDetails() {
        return transitDetails;
    }

    public void setTransitDetails(TransitDetails transitDetails) {
        this.transitDetails = transitDetails;
    }

    public String getTravelMode() {
        return travelMode;
    }

    public void setTravelMode(String travelMode) {
        this.travelMode = travelMode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(distance, i);
        parcel.writeParcelable(duration, i);
        parcel.writeParcelable(endLocation, i);
        parcel.writeString(htmlInstructions);
        parcel.writeParcelable(startLocation, i);
        parcel.writeParcelable(transitDetails, i);
        parcel.writeString(travelMode);
        parcel.writeTypedList(points);
        parcel.writeTypedList(step);
    }
}
