package com.vuitv.myapplication.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.vuitv.myapplication.Object.XeBus;
import com.vuitv.myapplication.R;

import java.util.ArrayList;

public class BusAdapter extends BaseAdapter {
    ArrayList<XeBus> listData;
    ArrayList<XeBus> tmpData;
    LayoutInflater inflater;

    public BusAdapter() {
    }

    public BusAdapter(ArrayList<XeBus> listData, Context context) {
        this.listData = listData;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_list_bus, parent, false);
        }

        XeBus xeBus = listData.get(position);
        TextView txtMa, txtTenXe;
        txtMa = (TextView) convertView.findViewById(R.id.txtMa);
        txtTenXe = (TextView) convertView.findViewById(R.id.txtTenXe);
        txtMa.setText(xeBus.getMa());
        txtTenXe.setText(xeBus.getTenxe());

        return convertView;
    }

    public Filter getFilter() {
        if (tmpData == null)
            tmpData = listData;
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listData = (ArrayList<XeBus>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                ArrayList<XeBus> FilteredList = new ArrayList<>();
                if (constraint == null || constraint.length() == 0) {
                    results.values = tmpData;
                    results.count = tmpData.size();
                } else {
                    for (int i = 0; i < tmpData.size(); i++) {
                        XeBus data = tmpData.get(i);
                        if (data.getTenxe().toLowerCase().contains(constraint.toString())
                                || data.getMa().toLowerCase().contains(constraint.toString())) {
                            FilteredList.add(data);
                        }
                    }
                    results.values = FilteredList;
                    results.count = FilteredList.size();
                }
                return results;
            }
        };
        return filter;
    }
}
