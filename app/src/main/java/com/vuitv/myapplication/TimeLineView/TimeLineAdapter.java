package com.vuitv.myapplication.TimeLineView;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.vuitv.myapplication.R;

import java.util.List;


public class TimeLineAdapter extends RecyclerView.Adapter<TimeLineViewHolder> {

    private List<String> mFeedList;
    private Context mContext;
    private Orientation mOrientation;

    public TimeLineAdapter(List<String> feedList, Orientation orientation) {
        mFeedList = feedList;
        mOrientation = orientation;
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, getItemCount());
    }

    @Override
    public TimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        mContext = parent.getContext();

        View view;

        if (mOrientation == Orientation.horizontal) {
            view = View.inflate(parent.getContext(), R.layout.item_maps_bus, null);
        } else {
            view = View.inflate(parent.getContext(), R.layout.item_maps_bus, null);
        }

        return new TimeLineViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(TimeLineViewHolder holder, int position) {

        String timeLineModel = mFeedList.get(position);

        holder.name.setText(timeLineModel);

    }

    @Override
    public int getItemCount() {
        return (mFeedList != null ? mFeedList.size() : 0);
    }

}
