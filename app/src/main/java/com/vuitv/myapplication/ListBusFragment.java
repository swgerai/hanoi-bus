package com.vuitv.myapplication;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.vuitv.myapplication.Adapter.BusAdapter;
import com.vuitv.myapplication.Object.XeBus;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListBusFragment extends Fragment {


    int cout = 20;
    private EditText editText;
    private ArrayList<XeBus> lsData = new ArrayList<>();
    private ListView lsView;
    private BusAdapter adapter;
    public ListBusFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_bus, container, false);

        lsView = (ListView) view.findViewById(R.id.lsView);
        adapter = new BusAdapter(lsData, getActivity());
        lsView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        MainActivity.database.limitToFirst(cout).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                lsData.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    XeBus xeBus = snapshot.getValue(XeBus.class);
                    lsData.add(xeBus);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        lsView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                if (absListView.getId() == lsView.getId()) {
                    MainActivity.database.limitToFirst(cout).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            lsData.clear();
                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                XeBus xeBus = snapshot.getValue(XeBus.class);
                                lsData.add(xeBus);
                            }
                            adapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
                cout += 10;
            }
        });


        lsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), DetailBusActivity.class);
                intent.putExtra("xebus", lsData.get(i));
                startActivity(intent);
            }
        });
        editText = (EditText) view.findViewById(R.id.edSearch);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                adapter.getFilter().filter(editable);
            }
        });

        return view;
    }


    public void hideSoftKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onPause() {
        hideSoftKeyboard(getView());
        super.onPause();
    }
}
