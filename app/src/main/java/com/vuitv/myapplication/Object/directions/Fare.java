package com.vuitv.myapplication.Object.directions;

public class Fare {
    private String currency;
    private String text;
    private String value;

    public Fare() {
    }

    public Fare(String currency, String text, String value) {
        this.currency = currency;
        this.text = text;
        this.value = value;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
