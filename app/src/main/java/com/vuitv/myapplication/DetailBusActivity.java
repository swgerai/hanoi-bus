package com.vuitv.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.vuitv.myapplication.Object.XeBus;

public class DetailBusActivity extends AppCompatActivity {

    TextView txtMa, txtTenxe, txtXinghiep, txtGiave, txtTanxuat;
    TextView txtThoigian, txtChieudi, txtChieuve, txtSoxe;

    XeBus xeBus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_bus);

        initView();
        xeBus = getIntent().getParcelableExtra("xebus");

        txtMa.setText(xeBus.getMa());
        txtTenxe.setText(xeBus.getTenxe());
        txtXinghiep.setText(xeBus.getXinghiep());
        txtTanxuat.setText(xeBus.getTanxuat());
        txtGiave.setText(xeBus.getGiave());
        txtSoxe.setText(xeBus.getSoxe());
        txtThoigian.setText(xeBus.getThoigian());
        txtChieudi.setText(xeBus.getChieudi());
        txtChieuve.setText(xeBus.getChieuve());

    }

    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Thông tin tuyến bus");

        txtMa = (TextView) findViewById(R.id.txtMa);
        txtTenxe = (TextView) findViewById(R.id.txtTenxe);
        txtXinghiep = (TextView) findViewById(R.id.txtXinghiep);
        txtGiave = (TextView) findViewById(R.id.txtGiave);
        txtTanxuat = (TextView) findViewById(R.id.txtTanxuat);
        txtThoigian = (TextView) findViewById(R.id.txtThoigian);
        txtSoxe = (TextView) findViewById(R.id.txtSoxe);
        txtChieudi = (TextView) findViewById(R.id.txtChieudi);
        txtChieuve = (TextView) findViewById(R.id.txtChieuve);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bus, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.actionMaps:
                Intent intent = new Intent(this, MapsBusActivity.class);
                intent.putExtra("bus", xeBus);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
