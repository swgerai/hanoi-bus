package com.vuitv.myapplication.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.vuitv.myapplication.Object.directions.Routes;
import com.vuitv.myapplication.Object.directions.Steps;
import com.vuitv.myapplication.R;

import java.util.List;

public class DetailDirectionAdapter extends BaseAdapter {
    private Routes routes;
    private List<Steps> lsData;
    private LayoutInflater inflater;

    public DetailDirectionAdapter() {
    }

    public DetailDirectionAdapter(Routes routes, Context context) {
        this.routes = routes;
        this.lsData = routes.getSteps();
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return lsData.size();
    }

    @Override
    public Object getItem(int i) {
        return lsData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_detail_direction, viewGroup, false);
        }
        ImageView img = (ImageView) view.findViewById(R.id.img);
        TextView txtStartTo, txtStart, txtFromStop, txtStop, txtDistance, txtDuration;
        txtStartTo = (TextView) view.findViewById(R.id.txtStartTo);
        txtStart = (TextView) view.findViewById(R.id.txtStart);
        txtFromStop = (TextView) view.findViewById(R.id.txtFromStop);
        txtStop = (TextView) view.findViewById(R.id.txtStop);
        txtDistance = (TextView) view.findViewById(R.id.txtDistance);
        txtDuration = (TextView) view.findViewById(R.id.txtDuration);

        Steps steps = lsData.get(i);
        if (steps.getTravelMode().equals("WALKING")) {
            img.setImageResource(R.drawable.ic_walk_blue_24dp);
            if (i == 0) {
                txtStartTo.setText("Bắt đầu từ");
                txtStart.setText(routes.getStartAddress().substring(0, routes.getStartAddress().indexOf(", ")));
            } else if (i == lsData.size() - 1) {
                txtStartTo.setText("Từ điểm dừng");
                txtStart.setText(routes.getEndAddress().substring(0, routes.getEndAddress().indexOf(", ")));
            } else {
                txtStartTo.setText("Từ điểm dừng");
                txtStart.setText("");
            }
            txtStart.setTextColor(txtStop.getTextColors());
            txtFromStop.setText("Đi bộ đến");
            txtStop.setText(steps.getHtmlInstructions());
            txtDistance.setText(steps.getDistance().getText());
            txtDuration.setText(steps.getDuration().getText());
        }
        if (steps.getTravelMode().equals("TRANSIT")) {
            img.setImageResource(R.drawable.ic_directions_bus_blue_24px);
            txtStartTo.setText("Chờ xe");
            txtStart.setText(steps.getTransitDetails().getLineName());
            txtStart.setTextColor(Color.RED);
            txtFromStop.setText("Đi xe đến");
            txtStop.setText(steps.getTransitDetails().getArrivalStop().getName());
            txtDistance.setText(steps.getDistance().getText());
            txtDuration.setText(steps.getDuration().getText());
        }
        return view;
    }
}
