package com.vuitv.myapplication.Object.directions;

import android.os.Parcel;
import android.os.Parcelable;

public class TransitDetails implements Parcelable {
    public static final Creator<TransitDetails> CREATOR = new Creator<TransitDetails>() {
        @Override
        public TransitDetails createFromParcel(Parcel in) {
            return new TransitDetails(in);
        }

        @Override
        public TransitDetails[] newArray(int size) {
            return new TransitDetails[size];
        }
    };
    private ArrivalStop arrivalStop;
    private ArrivalStop departureStop;
    private String arrivalTime;
    private String departureTime;
    private String headsign;
    private String lineName;
    private int numStops;

    public TransitDetails() {
    }

    protected TransitDetails(Parcel in) {
        arrivalStop = in.readParcelable(ArrivalStop.class.getClassLoader());
        departureStop = in.readParcelable(ArrivalStop.class.getClassLoader());
        arrivalTime = in.readString();
        departureTime = in.readString();
        headsign = in.readString();
        lineName = in.readString();
        numStops = in.readInt();
    }

    public ArrivalStop getArrivalStop() {
        return arrivalStop;
    }

    public void setArrivalStop(ArrivalStop arrivalStop) {
        this.arrivalStop = arrivalStop;
    }

    public ArrivalStop getDepartureStop() {
        return departureStop;
    }

    public void setDepartureStop(ArrivalStop departureStop) {
        this.departureStop = departureStop;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getHeadsign() {
        return headsign;
    }

    public void setHeadsign(String headsign) {
        this.headsign = headsign;
    }

    public String getLineName() {
        return lineName;
    }

    public void setLineName(String lineName) {
        this.lineName = lineName;
    }

    public int getNumStops() {
        return numStops;
    }

    public void setNumStops(int numStops) {
        this.numStops = numStops;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(arrivalStop, i);
        parcel.writeParcelable(departureStop, i);
        parcel.writeString(arrivalTime);
        parcel.writeString(departureTime);
        parcel.writeString(headsign);
        parcel.writeString(lineName);
        parcel.writeInt(numStops);
    }
}
