package com.vuitv.myapplication.TimeLineView;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.vuitv.myapplication.R;


public class TimeLineViewHolder extends RecyclerView.ViewHolder {
    public TextView name;
    public TimelineView mTimelineView;

    public TimeLineViewHolder(View itemView, int viewType) {
        super(itemView);
        name = (TextView) itemView.findViewById(R.id.tvName);
        mTimelineView = (TimelineView) itemView.findViewById(R.id.time_marker);
        mTimelineView.initLine(viewType);
    }
}
