package com.vuitv.myapplication;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vuitv.myapplication.Object.DiemDon;
import com.vuitv.myapplication.Object.XeBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


public class MainActivity extends AppCompatActivity {
    public static DatabaseReference database = FirebaseDatabase.getInstance().getReference("thongtinxebus");
    public static Set<String> lsPoint = new HashSet<>();
    public static Map<String, DiemDon> mpDiemDon = new HashMap<>();
    boolean checkDoubleBack = false;
    private BottomNavigationView bottomBar;
    private Fragment fragment;
    private ArrayList<XeBus> lsBus = new ArrayList<>();

    public static <T> Set<T> mergeSet(final Set<T> a, final Set<T> b)
    {
        return new HashSet<T>() {{
            addAll(a);
            addAll(b);
        }};
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomBar = (BottomNavigationView) findViewById(R.id.bottomBar);

        fragment = new ListBusFragment();
        final FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.contentPanel, fragment).commit();

        bottomBar.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.actionTimDuong:
                        fragment = new HomeFragment();
                        break;
                    case R.id.actionTraCuu:
                        fragment = new ListBusFragment();
                        break;
                }
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.contentPanel, fragment).commit();
                return true;
            }
        });

        HomeFragment.min = 11;
        HomeFragment.max = 12;
        reloadData(11,12);
    }

    public void reloadData(final int min, final int max) {
        lsBus.clear();
        mpDiemDon.clear();
        lsPoint.clear();
        database.limitToFirst(200).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (lsBus.size() != 0) {
                    return;
                }
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    XeBus xeBus = snapshot.getValue(XeBus.class);
                    if (xeBus.getMa().equals("09")) {
                        continue;
                    }

                    lsBus.add(xeBus);
                    if (min != 0 && max != 0) {
                        boolean found = false;
                        try {
                            int tuyen = Integer.parseInt(xeBus.getMa());
                            if (tuyen >= min && tuyen <= max) {
                                found = true;
                            }
                        } catch (Exception ex) {

                        }
                        if (!found) {
                            continue;
                        }
                    }
                    String temp = xeBus.getChieudi();
                    String[] temps = temp.split("-");
                    DiemDon[] points = new DiemDon[temps.length];
                    for (int i = 0; i < temps.length; i++) {
                        String tendiem = temps[i].split("[\\(\\)]")[0].trim();
                        if (tendiem.contains("uay đầu") || tendiem.contains("ẽ phải") || tendiem.contains("ẽ trái")) {
                            points[i] = points[i - 1];
                            continue;
                        }
                        DiemDon d = mpDiemDon.getOrDefault(tendiem, null);
                        if (d == null) {
                            d = new DiemDon(tendiem);
                        }
                        points[i] = d;

                        if (i != 0) {
                            d.addTuyenden(xeBus.getMa());
                            d.addDitu(points[i - 1]);
                            points[i - 1].addDiden(d);
                        }
                        if (i != temps.length - 1) {
                            d.addTuyendi(xeBus.getMa());
                        }

                        mpDiemDon.put(d.getTendiem(), d);
                    }
                    temp = xeBus.getChieuve();
                    temps = temp.split("-");
                    points = new DiemDon[temps.length];
                    for (int i = 0; i < temps.length; i++) {
                        String tendiem = temps[i].split("[\\(\\)]")[0].trim();
                        if (tendiem.contains("uay đầu") || tendiem.contains("ẽ phải") || tendiem.contains("ẽ trái")) {
                            points[i] = points[i - 1];
                            continue;
                        }
                        DiemDon d = mpDiemDon.getOrDefault(tendiem, null);
                        if (d == null) {
                            d = new DiemDon(tendiem);
                        }
                        points[i] = d;

                        if (i != 0) {
                            d.addTuyenden(xeBus.getMa());
                            d.addDitu(points[i - 1]);
                            points[i - 1].addDiden(d);
                        }
                        if (i != temps.length - 1) {
                            d.addTuyendi(xeBus.getMa());
                        }

                        mpDiemDon.put(d.getTendiem(), d);
                    }
                }
                for (Map.Entry<String, DiemDon> entry : mpDiemDon.entrySet()) {
                    StringBuilder sb = new StringBuilder(entry.getKey());
                    sb.append(" [ ");
                    for (String s : mergeSet(entry.getValue().getTuyenden(), entry.getValue().getTuyendi())) {
                        sb.append(s).append(" ");
                    }
                    sb.append("]");
                    lsPoint.add(sb.toString());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (checkDoubleBack) finish();
        this.checkDoubleBack = true;
        Toast.makeText(this, "Nhấn lần nữa để thoát", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                checkDoubleBack = false;
            }
        }, 2000);
    }

}
